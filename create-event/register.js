/*
 *
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ext from '@/io.ox/core/extensions'
import AddParticipantView from '@/io.ox/participants/add'
import { settings as coreSettings } from '@/io.ox/core/settings'
import { settings } from '@/io.ox/calendar/settings'
import gt from 'gettext'
import '@/fr.dinum/create-event/style.scss'

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

function randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function generateRoomName() {
    var charArray = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    var digitArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    var roomName = shuffle(digitArray).join('').substring(0, randomIntFromInterval(3, 6)) + shuffle(charArray).join('').substring(0, randomIntFromInterval(7, 10));
    return shuffle(roomName.split('')).join('');
}

ext.point('io.ox/calendar/edit/section').extend({
    id: 'create-webconf',
    index: 780,
    draw: function () {
        this.append(
            $('<div class="find-free-time">')
                .append($('<button type="button" class="btn btn-link mb-4" data-action="create-webconf">')
                .text('Créer un lien webconf')
                .on('click', function (e) {
                    var url = 'https://webconf.numerique.gouv.fr/' + generateRoomName();
                    e.stopPropagation();
                    var target =  $(document).find('.form-control[name="location"]');
                    target.val('Lien vers la webconf : ' + url);
                    target.focus();
                })
            )
        );
    }
});

// Modify the participant section to not add resources
ext.point('io.ox/calendar/edit/section').extend({
    id: 'add-participant-ext',
    index: 890,
    rowClass: 'collapsed',
    draw: function (baton) {
        baton.parentView.addParticipantsView = new AddParticipantView({
      apiOptions: {
        contacts: true,
        users: true,
        groups: true,
        resources: false,
        distributionlists: true
      },
      convertToAttendee: true,
      collection: baton.model.getAttendees(),
      blocklist: settings.get('participantBlacklist') || false,
      scrollIntoView: true,
      // to prevent addresspicker from processing data asynchronously.
      // Not needed and may cause issues with slow network (hitting save before requests return).
      processRaw: true,
      label: gt('Participants'),
      labelVisible: true,
      placeholder: gt('Name or email address') + ' ...'
    })
        console.log(AddParticipantView);
        // e.stopPropagation();
        return false;
    }
})

// Add Resources section separatly
ext.point('io.ox/calendar/edit/section').extend({
    id: 'add-ressources',
    index: 950,
    draw: function (baton) {
    if (baton.parentView.options.usedGroups) baton.model.getAttendees().usedGroups = _.uniq((baton.model.getAttendees().usedGroups || []).concat(baton.parentView.options.usedGroups))

    const add = new AddParticipantView({
      apiOptions: {
        contacts: false,
        users: false,
        groups: false,
        resources: true,
        distributionlists: false
      },
      convertToAttendee: true,
      collection: baton.model.getAttendees(),
      blocklist: settings.get('participantBlacklist') || false,
      scrollIntoView: true,
      processRaw: false,
      label: gt('Resources'),
      labelVisible: true,
      placeholder: gt('Nom de la ressource, de la salle') + ' ...'
    })

    this.append(
      add.render().$el
        .addClass('col-xs-12 pb-8 add-ressources sticky top-0 z-10')
    )
   
  }
})

